Sample to illustrate content package dependencies for setup in IntelliJ
=====

(see thread at http://devnet.jetbrains.com/message/5505351)

Both packagedeps-consumer and packagedeps-provider have been built from Adobe's simple-content-package-archetype:
http://dev.day.com/docs/en/cq/current/core/how_to/how_to_use_the_vlttool/vlt-mavenplugin.html#simple-content-package-archetype

provider contains a JSP src/main/content/jcr_root/apps/provider/global.jsp that defines objects.

consumer contains a JSP src/main/content/jcr_root/apps/consumer/components/sample/sample.jsp that includes /apps/provider/global.jsp and uses the objects defined therein.

consumer contains a dependency on provider. the setup is not a reactor, as this is trying to illustrate how to work with project external dependencies towards content packages.

If both results are deployed into Sling or AEM, the inclusion works well.

The IntelliJ part
-----------------

IntelliJ resolves inclusions within a project without a problem (even without setting up a Web facet).
However, the dependency on provider in the consumer project is not resolved in a way that allows the inclusion of /apps/provider/global.jsp to be resolved, so that the objects remain unresolved.